<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        INFO GOES 
        <ul id="photographers">

        </ul>
    </div>
</body>
<script>

    getData('http://localhost:8000/api/photographer');

    async function getData(url) {
        const response = await fetch(url);
        const json = await response.json();
        console.log(json);
        let listItems = '';
        for(var i = 0; i < json.length; i++){
             listItems += "<li>"+json[i].name+"</li>";
        }

        document.getElementById('photographers').innerHTML = listItems;

                
    }

</script>
</html>