import React from "react";

function Photographer(props) {
    return (
        <div className="jumbotron row">
            <div className="col-md-2">
                <img
                    className="img-thumbnail rounded-circle"
                    src={"/storage/" + props.data.profile_picture}
                    alt={props.data.name}
                />
            </div>
            <div className="col-md-7 ">
                <h3 className="display-4 font-weight-bold">
                    {props.data.name}
                </h3>

                <b className="h3 font-weight-bold">Bio</b>
                <br />
                <span className="h3 font-weight-normal">{props.data.bio}</span>
            </div>
            <div className="col-md-3 h3 pt-5">
                <b>Phone</b>
                <br />
                <span style={{ color: "red", fontWeight: "600" }}>
                    {props.data.phone}
                </span>
                <br />
                <b>Email</b>
                <br />
                <span style={{ color: "red", fontWeight: "600" }}>
                    {props.data.email}
                </span>
            </div>
        </div>
    );
}

export default Photographer;
