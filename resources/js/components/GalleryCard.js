import React from "react";
import { data } from "jquery";

function GalleryCard(props) {
    return (
        <div className="card mb-4 shadow-sm" style={{ height: "auto" }}>
            <div style={{ position: "relative" }} className="text-center">
                <div
                    style={{
                        position: "absolute",
                        bottom: ".75vw",
                        left: ".75vw",
                        fontSize: "3vh",
                        fontWeight: "700",
                        color: "white"
                    }}
                >
                    {props.data.title}
                </div>
                <img
                    className="img-responsive"
                    src={"/storage/" + props.data.url_to_image}
                    alt={props.data.title}
                />
            </div>

            <div className="card-body">
                <p className="card-text h3">{props.data.description}</p>
                <div className="d-flex justify-content-between align-items-center">
                    <i
                        className={
                            props.data.featured
                                ? "fa fa-heart text-danger h4 "
                                : "d-inline-block"
                        }
                    ></i>
                    <small className="text-muted h4">{props.data.date}</small>
                </div>
            </div>
        </div>
    );
}

export default GalleryCard;
