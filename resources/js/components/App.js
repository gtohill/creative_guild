import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import Photographer from "./Photographer";
import GalleryCard from "./GalleryCard";

function App() {
    const [gallery, setGallery] = useState([]);
    const [photographer, setPhotographer] = useState({});

    useEffect(() => {
        fetchData();
    }, []);

    let x = "";
    const fetchData = async () => {
        const data = await fetch("/api/photographer");
        const item = await data.json();
        setPhotographer(item[0]);
        setGallery(item[1]);               
    };

    return (
        <div className="container" style={{ wordWrap: "break-word" }}>
            <Photographer data={photographer} />

            <div className="row justify-content-center">
                {gallery.map(item => (
                    <div className="col-md-4" key={item.id}>
                        <GalleryCard data={item} />
                    </div>
                ))}
            </div>
        </div>
    );
}

export default App;

if (document.getElementById("app")) {
    ReactDOM.render(<App />, document.getElementById("app"));
}
