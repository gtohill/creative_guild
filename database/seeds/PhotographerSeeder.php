<?php

use Illuminate\Database\Seeder;
use App\Photographer;

class PhotographerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photographer = new Photographer();
                        
        $photographer->name = "Nick Reynolds";
        $photographer->phone = "555-555-5555";
        $photographer->email = "nick.reynolds@domain.co";
        $photographer->bio =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
        $photographer->profile_picture = "img/profile.jpeg";
        $photographer->save();
       
    }
}
