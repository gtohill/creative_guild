<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $galleris_to_seed_json =  '[
            {
              "id":1,
              "title": "Nandhaka Pieris",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape1.jpeg",
              "date": "2015-05-01",
              "featured": true
            },
            {
              "id":2,
              "title": "New West Calgary",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape2.jpeg",
              "date": "2016-05-01",
              "featured": false
            },
            {
              "id":3,
              "title": "Australian Landscape",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape3.jpeg",
              "date": "2015-02-02",
              "featured": false
            },
            {
              "id":4,
              "title": "Halvergate Marsh",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape4.jpeg",
              "date": "2014-04-01",
              "featured": true
            },
            {
              "id":5,
              "title": "Rikkis Landscape",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape5.jpeg",
              "date": "2010-09-01",
              "featured": false
            },
            {
              "id":6,
              "title": "Kiddi Kristjans Iceland",
              "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
              "img": "img/landscape6.jpeg",
              "date": "2015-07-21",
              "featured": true
            }
          ]';

        $galleries_to_seed_array = json_decode($galleris_to_seed_json);

        foreach($galleries_to_seed_array as $item){
            $gallery = new App\Gallery();
            $photo = new App\Photo();
            $gallery->photographer_id = 1;
            $gallery->title = $item->title;
            $gallery->description = $item->description;
            $gallery->date = $item->date;
            $gallery->featured = $item->featured;
            //$gallery->img = $item->img;
            $photo->url_to_image = $item->img;
            $photo->photographer_id = 1;
            $gallery->save();
            $gallery->photos()->save($photo);

        }


        
    }
}
