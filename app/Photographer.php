<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photographer extends Model
{
    protected $fillable = 
    [
        'name',
        'phone',
        'email',
        'bio',
        'profile_image',        
    ];

    public function galleries(){
        return $this->hasMany(Gallery::class);
    }

    public function photos(){
        return $this->hasMany(Photo::class);
    }
}
