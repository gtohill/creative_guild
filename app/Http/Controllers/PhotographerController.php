<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Gallery;
use Illuminate\Http\Request;
use App\Photographer;

class PhotographerController extends Controller
{
    /* Displays Nick Reynolds gallery
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $id = 1; //Nick Reynolds Id
        // get Nicks details and galleries
        $photographer = Photographer::findorfail($id);     
        //$photographer->galleries;
        $gallery = DB::table('galleries')            
            ->join('photos', 'galleries.id', '=', 'photos.gallery_id')
            ->select('*')
            ->get();
        
        
        return response()->json([$photographer, $gallery]);
    }

    /**
     * ack up Index method should seeding the database fail.  Please uncomment the index() function immediately below.
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     // get list of all photographers
    //     // $photographers = Photographer::all();
    //     $photographers = '{
    //         "name": "Nick Reynolds",
    //         "phone": "555-555-5555",
    //         "email": "nick.reynolds@domain.co",
    //         "bio": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    //         "profile_picture": "img/profile.jpeg",
    //         "album" : [
    //           {
    //             "id":1,
    //             "title": "Nandhaka Pieris",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape1.jpeg",
    //             "date": "2015-05-01",
    //             "featured": true
    //           },
    //           {
    //             "id":2,
    //             "title": "New West Calgary",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape2.jpeg",
    //             "date": "2016-05-01",
    //             "featured": false
    //           },
    //           {
    //             "id":3,
    //             "title": "Australian Landscape",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape3.jpeg",
    //             "date": "2015-02-02",
    //             "featured": false
    //           },
    //           {
    //             "id":4,
    //             "title": "Halvergate Marsh",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape4.jpeg",
    //             "date": "2014-04-01",
    //             "featured": true
    //           },
    //           {
    //             "id":5,
    //             "title": "Rikkis Landscape",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape5.jpeg",
    //             "date": "2010-09-01",
    //             "featured": false
    //           },
    //           {
    //             "id":6,
    //             "title": "Kiddi Kristjans Iceland",
    //             "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //             "img": "img/landscape6.jpeg",
    //             "date": "2015-07-21",
    //             "featured": true
    //           }
    //         ]
    //       }';

    //     return response()->json(json_decode($photographers));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified photographer gallery.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photographer_gallery = Photographer::findOrFail($id);

        return response()->json($photographer_gallery->galleries);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
