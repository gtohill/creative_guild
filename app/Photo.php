<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = 
    [
        'url_to_image',
        'created_at',                
    ];

    public function galleries(){
        return $this->belongsTo(Gallery::class);
    }

    public function photographer(){
        return $this->belongsTo(Photographer::class);
    }
}
