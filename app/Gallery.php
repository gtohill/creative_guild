<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = 
    [
        'title',
        'description',
        'created_at',
        'is_featured',
        'featured_image',        
    ];

    public function photos(){
        return $this->hasMany(Photo::class);
    }

    public function photographer(){
        return $this->belongsTo(Photographer::class);
    }
}
